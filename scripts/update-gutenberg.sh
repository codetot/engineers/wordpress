#!/bin/bash

BRANCH_NAME=$(git name-rev --name-only HEAD)
if [[ "$BRANCH_NAME" != "gutenberg" ]]; then
  echo "Your current branch is ${BRANCH_NAME}. We must checkout branch gutenberg branch before continuing."
  echo "Please manual reset to branch gutenberg. Committing any live changes to this branch and push to origin."
else
  echo "Your current branch is gutenberg. Start updating child repositories."
  git submodule foreach --recursive 'git fetch origin && git checkout gutenberg && git reset --hard origin/gutenberg'
  echo -e "All repositories have been reset to gutenberg branch."
fi



