<?php return array (
  'root' => 
  array (
    'pretty_version' => 'dev-master',
    'version' => 'dev-master',
    'aliases' => 
    array (
    ),
    'reference' => 'b718d086c5afe2b18217798ad874f161bddb6905',
    'name' => 'meta-box/meta-box-aio',
  ),
  'versions' => 
  array (
    'composer/installers' => 
    array (
      'pretty_version' => 'v1.10.0',
      'version' => '1.10.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '1a0357fccad9d1cc1ea0c9a05b8847fbccccb78d',
    ),
    'gamajo/template-loader' => 
    array (
      'pretty_version' => '1.3.1',
      'version' => '1.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fa92a37b780d945463f7fea328dce14933558752',
    ),
    'meta-box/mb-admin-columns' => 
    array (
      'pretty_version' => '1.5.0',
      'version' => '1.5.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '8f933234975659f5da6920dadce3d8516c52fea2',
    ),
    'meta-box/mb-blocks' => 
    array (
      'pretty_version' => '1.2.4',
      'version' => '1.2.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd8fb4516a1fea24c21f6aec8356f3d87f88e2e7b',
    ),
    'meta-box/mb-custom-table' => 
    array (
      'pretty_version' => '1.1.12',
      'version' => '1.1.12.0',
      'aliases' => 
      array (
      ),
      'reference' => '72035d27345d92f520a09c1fdd427f95fab7815d',
    ),
    'meta-box/mb-frontend-submission' => 
    array (
      'pretty_version' => '3.0.1',
      'version' => '3.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '2b0b26e4c2f65ac75729fa12ce5cd8cc62eeee6e',
    ),
    'meta-box/mb-revision' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '9999999-dev',
      ),
      'reference' => '0f108167e446f794cd23ef702f902e06d98a929e',
    ),
    'meta-box/mb-settings-page' => 
    array (
      'pretty_version' => '2.1.3',
      'version' => '2.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'df4c81819571bf54f8503a9e00a96d634afaf247',
    ),
    'meta-box/mb-term-meta' => 
    array (
      'pretty_version' => '1.2.10',
      'version' => '1.2.10.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bacaceaa0382ea5afeb0d23a1885c7823b4848b8',
    ),
    'meta-box/mb-user-meta' => 
    array (
      'pretty_version' => '1.2.8',
      'version' => '1.2.8.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e63c3a8a2541f93af9aad64911eee893cfe0de8e',
    ),
    'meta-box/mb-user-profile' => 
    array (
      'pretty_version' => '1.7.5',
      'version' => '1.7.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '0b85920662c23c0dd86a20375b982cc718a75cc2',
    ),
    'meta-box/mb-views' => 
    array (
      'pretty_version' => '1.8.2',
      'version' => '1.8.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '7a8112c4dec8b76fcbf5d37f7ba3f59e35ae916f',
    ),
    'meta-box/mbb-parser' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '9999999-dev',
      ),
      'reference' => 'f5b02a47842e839288a865c77694faf3e1708cea',
    ),
    'meta-box/meta-box-aio' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
      ),
      'reference' => 'b718d086c5afe2b18217798ad874f161bddb6905',
    ),
    'meta-box/meta-box-builder' => 
    array (
      'pretty_version' => '4.0.2',
      'version' => '4.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '350fd6b1e1cea022c4ae002bead1d12035be6122',
    ),
    'meta-box/meta-box-columns' => 
    array (
      'pretty_version' => '1.2.7',
      'version' => '1.2.7.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f424925e0f2a1c51c2a867ee43217195bb9c02ca',
    ),
    'meta-box/meta-box-conditional-logic' => 
    array (
      'pretty_version' => '1.6.13',
      'version' => '1.6.13.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e5b169e827cbb8421ee97a1ff058a792246b14e0',
    ),
    'meta-box/meta-box-geolocation' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '9999999-dev',
      ),
      'reference' => 'e0c1aed305e493a7fb77d6a72a99733606d3e822',
    ),
    'meta-box/meta-box-group' => 
    array (
      'pretty_version' => '1.3.11',
      'version' => '1.3.11.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c9a703621db056d3ed4bb5a225114037ba407e8f',
    ),
    'meta-box/meta-box-include-exclude' => 
    array (
      'pretty_version' => '1.0.11',
      'version' => '1.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '0eb07aca18e704b3b31c0bdfa1091c6dc65250b4',
    ),
    'meta-box/meta-box-show-hide' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '9999999-dev',
      ),
      'reference' => 'a2fed27200fcd949e96b645bcc4ad72e41ea5c3b',
    ),
    'meta-box/meta-box-tabs' => 
    array (
      'pretty_version' => '1.1.8',
      'version' => '1.1.8.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ab508261f0fad33f180f9e86da670554328fda9f',
    ),
    'meta-box/meta-box-template' => 
    array (
      'pretty_version' => '1.2.0',
      'version' => '1.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '4dc17e24fe8fd63548c7f526cb9299deb51f3f5e',
    ),
    'meta-box/meta-box-tooltip' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '9999999-dev',
      ),
      'reference' => '6d373254c0a12031083d0633c13532513c8fdccf',
    ),
    'mustangostang/spyc' => 
    array (
      'pretty_version' => '0.6.3',
      'version' => '0.6.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '4627c838b16550b666d15aeae1e5289dd5b77da0',
    ),
    'riimu/kit-phpencoder' => 
    array (
      'pretty_version' => 'v2.4.1',
      'version' => '2.4.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ca6f004e1290aec7ef4bebf6c0807b30fcf981d7',
    ),
    'roundcube/plugin-installer' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'shama/baton' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'tgmpa/tgm-plugin-activation' => 
    array (
      'pretty_version' => '2.6.1',
      'version' => '2.6.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c626d0d91fc8ef24916e809c7b79eeafab1c1cac',
    ),
    'wpmetabox/mb-comment-meta' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '9999999-dev',
      ),
      'reference' => 'af1eea3a9ba7ef1e9ef692fc3762e3fe33f798c7',
    ),
    'wpmetabox/mb-custom-post-type' => 
    array (
      'pretty_version' => '2.0.8',
      'version' => '2.0.8.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cecd5d6b1e3d7653ddde5b98210af6ded5ff2d26',
    ),
    'wpmetabox/mb-elementor-integrator' => 
    array (
      'pretty_version' => '2.0.6',
      'version' => '2.0.6.0',
      'aliases' => 
      array (
      ),
      'reference' => '0f85c6304fbebe99be1141bcc8cd7b439df2f433',
    ),
    'wpmetabox/mb-relationships' => 
    array (
      'pretty_version' => '1.10.5',
      'version' => '1.10.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '6baf93c1d2c12d6e13dfc16993c54427dc26602e',
    ),
    'wpmetabox/mb-rest-api' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '9999999-dev',
      ),
      'reference' => 'c43e0c90cf3d1118dd63d8a254bae89dbf495a7f',
    ),
    'wpmetabox/mb-yoast-seo' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '9999999-dev',
      ),
      'reference' => '4722a6f4698c6976725bb4ec44fb6ad51e7c0b55',
    ),
    'wpmetabox/meta-box-beaver-themer-integrator' => 
    array (
      'pretty_version' => '1.4.0',
      'version' => '1.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '8fe0bed2dc06842cabbc42341ab7780584385a46',
    ),
    'wpmetabox/meta-box-facetwp-integrator' => 
    array (
      'pretty_version' => '1.0.3',
      'version' => '1.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a5b01bd88a796d2be3ecf214cc158060b9931274',
    ),
    'wpmetabox/text-limiter' => 
    array (
      'pretty_version' => '1.1.2',
      'version' => '1.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '0418747193e9bde665feaa9520b0712b13a58553',
    ),
  ),
);
