��    t      �      \      \     ]  (   v     �     �  
   �     �     �     �               )     2     7     D  ,   K     x       ?   �  D   �     	     +	     8	     A	  
   V	  5   a	  }   �	  	   
     
     ;
     G
     W
     p
     x
     ~
     �
     �
     �
     �
  
   �
     �
  
   �
     �
     �
  
   �
     �
     �
     �
            V   $     {     �  
   �  
   �     �     �     �  
   �     �     �     �                 
        %     7     F     [     x  
   �     �     �     �     �     �     �     �     �     �                 ,   &     S     a     w     �     �     �  J   �               9  :   N  ;   �     �     �     �     �     �     �               &     ,     2     E  "   S     v     ~     �     �  
   �     �  $  �     �  8   �          4     S     `     m  /   �  
   �  !   �     �     �     �     �  /        <     B     I  E   K  -   �  
   �     �     �     �  /     k   5     �     �  
   �     �  &   �       
         +     <     A     X     ^     d  
   v     �     �     �     �  	   �     �  
   �     �  %   �  U   	     _  &   v     �     �     �     �  
   �     �     �     �          &     ,     3     C     [     t     �     �  	   �  	   �  
   �     �  
   �     �     �       )   +     U     Y  
   ^     i     �  *   �     �  &   �     �          0     O  <   l     �  )   �     �  :   �  C   5     y     �     �      �  %   �     �               '     /     >     Q  '   h     �     �     �     �     �     �   Additional CSS Class(es) After first block (paragraph or heading) After first heading Always for Post Types Appearance Auto Auto Insert Auto Insert Table of Contents Background Color Before first heading Behavior Bold Border Color Bottom By default, items of contents will be hidden Cancel Center Click "Enable Processing" for headings processing in this post. Click "Enable TOC" for automatic add table of contents to this post. Click to override default value Color Scheme Contents Convert to lowercase Counter %s Creates a table of contents for your posts and pages. Creates a table of contents for your posts/pages. Works automatically or manually (via shortcode, Gutenberg block or widget). Customize Customize Table of Contents Dark Colors Decimal numbers Decimal numbers (nested) Default Depth Disable TOC Disabled Don't show again Edit Enable Enable TOC Enabled Extra Bold Extra Light Float Full Width General Header Hello! Hierarchical View Hover Link Color If the count of headings in the post is less, then table of contents is not displayed. Inherit from theme Items Font Size Label Hide Label Show Left Light Light Colors Link Color LuckyWP Plugins Medium Minimal Count of Headings Misc. None Normal Numeration Numeration Suffix Other Settings Overridden settings: Override Color Scheme Colors Position Post Types Processing Headings Rate the plugin Remind later Right Right without flow Roman numbers Roman numbers (nested) SEO Save Saved! Scroll Offset Top Settings Show Panel "Table of Contents" in Post Types Skip Headings Skip heading by level Skip heading by text Skip headings Smooth Scroll Smooth Scroll Offset Top Specify headings (one per line) to be excluded from the table of contents. Table of Contents Table of Contents Settings Thank you very much! The symbol will be added after numeration. For example, %s The table of contents will be automatic added to this post. Thin Title Title Color Title Font Size Title Font Weight Toggle Show/Hide Top Visited Link Color White Width Without numeration Without title Wrap table of contents with %s tag default empty from scheme hide not loaded show Project-Id-Version: LuckyWP Table of Contents
POT-Creation-Date: 2020-03-12 16:12+0300
PO-Revision-Date: 2021-02-19 07:49+0000
Last-Translator: 
Language-Team: Tiếng Việt
Language: vi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/
X-Poedit-Basepath: ..
Plural-Forms: nplurals=1; plural=0;
X-Poedit-KeywordsList: __;_;esc_html__
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: plugin/editorBlock/src
Report-Msgid-Bugs-To: 
X-Loco-Version: 2.5.0; wp-5.6.1 Class CSS bổ sung Sau đoạn đầu tiên (văn bản hoặc tiêu đề) Sau tiêu đề đầu tiên Luôn luôn cho các post type Hiển thị Tự động Chèn tự động Tự động chèn Mục lục vào bài viết Màu nền Trước tiêu đề đầu tiên Trạng thái Đậm Màu viền Dưới cùng Mặc định, các đầu mục sẽ bị ẩn Hủy Giữa C Chọn "Bật Mục lục" để tự động thêm vào bài viết. Click để ghi đè giá trị mặc định Tông màu Mục lục Chuyển thành chữ thường Bộ đếm %s Tạo Mục lục cho bài viết hoặc trang. Tạo Mục lục cho bài viết hoặc trang. Chèn tự động hoặc sử dụng shortcode hay widget. Tùy biến Tùy biến mục lục Màu tối Đánh số nguyên (1, 2) Đánh số (1, 2, hàng con 2.1, 2.2) Mặc định Độ sâu Ẩn mục lục Ẩn Không xem lại nữa Sửa Bật Bật Mục lục Đã bật Siêu đậm Siêu mỏng Float Tràn khung Cơ bản Đầu trang Xin chào! Xem theo các cấp Màu khi rê chuột vào liên kết Nếu số tiêu đề ít hơn, Mục lục trong bài viết sẽ không hiện ra. Lấy từ giao diện Kích thước chữ các đầu mục Chữ khi ẩn Chữ khi hiện Trái Mỏng Màu sáng Màu liên kết Plugin của LuckyWP Trung bình Số tiêu đề tối thiểu Khác Không Bình thường Đánh số mục lục Ký tự sau đánh số Thiết lập khác Ghi đè thiết lập: Thay thế tông màu Vị trí Post Type Áp dụng Đánh giá plugin Nhắc sau Phải Phải không có flow Chữ cái La Mã (I, II) Chữ cái La Mã (I, II, hàng con II.I) SEO Lưu Đã lưu! Khoảng cách chênh lệch Thiết lập Hiện khung "Mục lục" trong post type Bỏ qua tiêu đề Bỏ qua tiêu đề theo thứ hạng Bỏ qua tiêu đề theo chữ Bỏ qua tiêu đề Click trượt xuống mượt Khoảng cách chênh lệch Ghi rõ các tiêu đề sẽ bị loại khỏi mục lục Mục lục bài viết Thiết lập Mục lục cho bài viết Xin cảm ơn! Ký tự được thêm vào sau đánh số. Ví dụ: %s Mục lục sẽ được tự động thêm vào bài viết này. Mỏng nhất Tiêu đề Màu tiêu đề Kích thước chữ tiêu đề Độ đậm nhạt chữ tiêu đề Cho phép ẩn hiện Trên cùng Màu liên kết đã xem Trắng Chiều rộng Không đánh số Không có tiêu đề Bọc ngoài Mục lục với thẻ %s mặc định chưa thiết lập từ tông màu ẩn không tải hiện 