#!/bin/bash

BRANCH_NAME=$(git name-rev --name-only HEAD)
if [[ "$BRANCH_NAME" != "production" ]]; then
  echo "Your current branch ${BRANCH_NAME} is not production. We must checkout to branch production branch before continuing."
  echo "Please manual reset to branch production. Committing any live changes to this branch and push to origin."
else
  echo "Your current branch is production. Start updating child repositories."
  git submodule foreach --recursive 'git fetch origin && git checkout production && git reset --hard origin/production'
  echo -e "All repositories have been reset to production branch."
fi



