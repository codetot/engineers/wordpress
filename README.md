# WordPress - Code Tot Version

## Setup

1. Clone a git `git clone git@github.com:codetot-web/wordpress.git` (using Laragon or LocalWP (replace `public` folder after creating new app).
2. Navigate to folder
3. Setup `git submodule` to enable sync from all sub GitHub repositories.

```
$ git submodule init

# Response:
Submodule 'wp-content/mu-plugins' (git@github.com:codetot-web/mu-plugins.git) registered for path 'wp-content/mu-plugins'
Submodule 'wp-content/plugins/ct-blocks' (git@github.com:codetot-web/ct-blocks.git) registered for path 'wp-content/plugins/ct-blocks'
Submodule 'wp-content/plugins/ct-optimization' (git@github.com:codetot-web/ct-optimization.git) registered for path 'wp-content/plugins/ct-optimization'
Submodule 'wp-content/plugins/ct-pro-toolkit' (git@github.com:codetot-web/ct-pro-toolkit.git) registered for path 'wp-content/plugins/ct-pro-toolkit'
Submodule 'wp-content/themes/ct-child-theme' (git@github.com:codetot-web/ct-child-theme.git) registered for path 'wp-content/themes/ct-child-theme'
Submodule 'wp-content/themes/ct-theme' (git@github.com:codetot-web/ct-theme.git) registered for path 'wp-content/themes/ct-theme'
```

4. Download latest source to each submodule:

```
$ git submodule update

# Response:
Cloning into 'D:/Laragon-WebServer/www/dev/wp-content/mu-plugins'...
Cloning into 'D:/Laragon-WebServer/www/dev/wp-content/plugins/ct-blocks'...
Cloning into 'D:/Laragon-WebServer/www/dev/wp-content/plugins/ct-optimization'...
Cloning into 'D:/Laragon-WebServer/www/dev/wp-content/plugins/ct-pro-toolkit'...
Cloning into 'D:/Laragon-WebServer/www/dev/wp-content/themes/ct-child-theme'...
Cloning into 'D:/Laragon-WebServer/www/dev/wp-content/themes/ct-theme'...
Submodule path 'wp-content/mu-plugins': checked out 'a7ac4bd632535459aa54b10bcade0a3b935a77c0'
Submodule path 'wp-content/plugins/ct-blocks': checked out '0db98b1d113e5e7edeebe821bbb85672c5072002'
Submodule path 'wp-content/plugins/ct-optimization': checked out '5ec063654bdaca3c522437e4019b66f8eac15b9e'
Submodule path 'wp-content/plugins/ct-pro-toolkit': checked out '387e0a9dd7b75fb2074097c5bd3b5270b46c8a84'
Submodule path 'wp-content/themes/ct-child-theme': checked out '17e9c34f5105b64bcf83afe4aea2576e9a03dd67'
Submodule path 'wp-content/themes/ct-theme': checked out 'd7b08318715d85355ae9783ba84163363b532f69'
```

5. Install a WordPress website
6. Activate all plugins

## Submodule Structure (Themes and Plugins)

**ct-blocks** (plugin) - https://github.com/codetot-web/ct-blocks

> Using this plugin to manage all available blocks for Flexible page content.

**ct-pro-toolkit** (plugin) - https://github.com/codetot-web/ct-pro-toolkit

> Using to add extra settings to CT Theme's settings (such as remove footer copyright, update container width... and much more)

**mu-plugins** (multiple plugins) - https://github.com/codetot-web/mu-plugins

> Using to load required plugins and can't be disabled.

**ct-theme** (parent theme) - https://github.com/codetot-web/ct-theme

> A parent theme.

**ct-child-theme** (example child theme) - https://github.com/codetot-web/ct-child-theme

> The example of child theme to help developers learn how to extend more features, layouts and blocks.

## Checkout and update code

To ensure your code is up-to-date before commiting, please run `git submodule update` before each commit.

## Contribute Guideline

### Update submodule (theme or plugin)

- Step 1: Commit in submodule's folder. (for example, if you edit ct-theme, commit changes at `wp-content/themes/ct-theme`). Review and merge into `master`.
- Step 2: Navigate to wordpress folder, ensure submodule is on `master` branch, then commit to main repository `wordpress`.

**Example commit:**

 - ct-theme (master) - Update website settings - v1.7.1 and merge to branch `master`
 - wordpress (develop) - Merge v1.7.1 in `ct-theme`
