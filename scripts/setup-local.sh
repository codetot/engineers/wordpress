#!/bin/bash

git submodule init
git submodule update

git submodule foreach --recursive 'git fetch origin && git checkout master && git reset --hard origin/master'
echo -e "All repositories have been reset to master branch."