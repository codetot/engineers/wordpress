��          <      \       p   f   q   s   �   e   L  b  �  z     w   �  i                      Ensure that SeoPress analyzes Advanced Custom Fields content including Flexible Content and Repeaters. Please enable <strong>Advanced Custom Fields</strong> in order to use the plugin ACF Content Analysis For Seopress. Please enable <strong>SEOPress</strong> in order to use the plugin ACF Content Analysis For Seopress. Project-Id-Version: ACF Content Analysis For Seopress
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-09-24 12:30+0200
Language-Team: 
Plural-Forms: nplurals=2; plural=(n > 1);
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.4.1
X-Loco-Version: 2.4.3; wp-5.5.1
Last-Translator: 
Language: fr
 Assurez-vous que SeoPress analyse Advanced Custom Fields le contenu, y compris les contenus flexible et les répétitives. Veuillez activer <strong> Advanced Custom Fields </strong> afin d'utiliser le plugin ACF Content Analysis For Seopress. Veuillez activer <strong> SEOPress </strong> afin d'utiliser le plugin ACF Content Analysis For Seopress. 