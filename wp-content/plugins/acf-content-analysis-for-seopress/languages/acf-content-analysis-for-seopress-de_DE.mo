��          <      \       p   f   q   s   �   e   L    �  �   �  |   E  n   �                   Ensure that SeoPress analyzes Advanced Custom Fields content including Flexible Content and Repeaters. Please enable <strong>Advanced Custom Fields</strong> in order to use the plugin ACF Content Analysis For Seopress. Please enable <strong>SEOPress</strong> in order to use the plugin ACF Content Analysis For Seopress. Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-09-24 12:35+0200
Last-Translator: 
Language-Team: 
Language: de_DE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.4.1
X-Poedit-Basepath: .
 Stellen Sie sicher, dass SeoPress Inhalte für Advanced Custom Fields analysiert, einschließlich flexibler Inhalte und Repeaters. Bitte aktivieren Sie <strong>Advanced Custom Fields</ strong>, um das Plugin ACF Content Analysis For Seopress zu verwenden. Bitte aktivieren Sie <strong>SEOPress</ strong>, um das Plugin ACF Content Analysis For Seopress zu verwenden. 