#!/bin/bash

git config --global user.name "CODE TOT JSC"
git config --global user.email "dev@codetot.com"

git submodule init
git submodule update

git submodule foreach --recursive 'git fetch origin && git checkout -b production && git reset --hard origin/production && git branch -D master'
echo -e "All repositories have been reset to production branch."